class OverbridgeException(Exception):
    """Exception inherited by all overbridge exceptions"""
    def __str__(self):
        return self.__class__.__name__
