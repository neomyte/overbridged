from . import HEART, CLOVER, SPADE, DIAMOND, COLOR, VALUE
from .interpreter import read_card

class Hand():
    def __init__(self, cards):
        self._cards = cards

    @property
    def spades(self):
        return self._cards[SPADE]

    @property
    def hearts(self):
        return self._cards[HEART]

    @property
    def clovers(self):
        return self._cards[CLOVER]

    @property
    def diamonds(self):
        return self._cards[DIAMOND]

    @property
    def highcard_points(self):
        return sum(
            value - 10
            for color, values in self._cards.items()
            for value in values
            if value > 10
        )

    @property
    def length_points(self):
        return sum(
            len(values) - 4
            for color, values in self._cards.items()
            if len(values) > 4
        )

    @property
    def distribution_points(self):
        print(self._cards)
        print(sum(
            3 - len(values)
            for color, values in self._cards.items()
            if len(values) < 4
        ))
        return sum(
            3 - len(values)
            for color, values in self._cards.items()
            if len(values) < 4
        )

    def __len__(self):
        return sum(len(values) for color, values in self._cards.items())

    def __contains__(self, card):
        card = read_card(card)
        return len(card) == 2 and card[VALUE] in self._cards[card[COLOR]]