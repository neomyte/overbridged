from random import sample

from .deck import Deck
from .interpreter import read_card
from . import VALUES, COLORS

class DeckBuilder(Deck):
    @staticmethod
    def random():
        init = DeckBuilder.initial_deck_cards()
        return Deck(sample(init, k=len(init)))

    @staticmethod
    def initial_deck():
        return Deck(DeckBuilder.initial_deck_cards())

    @staticmethod
    def initial_deck_cards():
        return [
            read_card(f'{value}{color}')
            for color in COLORS
            for value in VALUES
        ]