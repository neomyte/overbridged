from . import (
    HEART,
    SPADE,
    CLOVER,
    DIAMOND,
    JACK,
    QUEEN,
    KING,
    ACE,
    TEN,
    COLOR_MAPPER,
    VALUE_MAPPER
)

def read_card(card):
    return {
        'color': COLOR_MAPPER[card[1]],
        'value': int(VALUE_MAPPER.get(card[0], card[0]))
    }