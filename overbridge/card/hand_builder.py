from __future__ import annotations

import yaml

from overbridge.utils.exception import OverbridgeException
from overbridge import logger
from . import HEART, CLOVER, SPADE, DIAMOND, VALUE_MAPPER, COLOR_MAPPER
from .hand import Hand

MAX_CARD_IN_HAND = 13

class HandBuilder(Hand):

    def __init__(self):
        self._cards = HandBuilder.empty_hand()

    def add_spades(self, spades: list) -> HandBuilder:
        return self.add_cards(SPADE, spades)

    def add_hearts(self, hearts: list) -> HandBuilder:
        return self.add_cards(HEART, hearts)

    def add_clovers(self, clovers: list) -> HandBuilder:
        return self.add_cards(CLOVER, clovers)

    def add_diamonds(self, diamonds: list) -> HandBuilder:
        return self.add_cards(DIAMOND, diamonds)

    def add_cards(self, color, values):
        size_after_add = len(values) + len(self)
        if(size_after_add > MAX_CARD_IN_HAND):
            print(len(values), len(self), size_after_add)
            raise TooManyCardException(size_after_add)
        self._cards[color] += [ int(VALUE_MAPPER.get(value, value)) for value in values ]
        return self

    def build(self):
        return Hand(self._cards)

    @staticmethod
    def empty_hand():
        return {
            SPADE:   [],
            CLOVER:  [],
            HEART:   [],
            DIAMOND: []
        }

    @staticmethod
    def from_file(filename, handname):
        with open(filename) as f:
            content = yaml.full_load(f)
        return Hand(
            {
                **HandBuilder.empty_hand(),
                **{
                    COLOR_MAPPER[serial[0]]: [
                        int(VALUE_MAPPER.get(value, value))
                        for value in serial[1:]
                    ]
                    for serial in content[handname].split(' ')
                }
            }
        )

class TooManyCardException(OverbridgeException):
    """Exception thrown when too many cards are put in a hand"""
    def __str__(self, nb_card):
        return logger.error(f'[{self.__class__.__name__}] Trying to have more than {MAX_CARD_IN_HAND} card in a hand ({nb_card})')
