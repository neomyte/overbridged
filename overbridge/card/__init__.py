HEART   = 'h'
CLOVER  = 'c'
SPADE   = 's'
DIAMOND = 'd'

TEN   = 10
JACK  = 11
QUEEN = 12
KING  = 13
ACE   = 14

COLOR  = 'color'
VALUE  = 'value'
COLORS = 'dchs'
VALUES = '23456789tjqka'

COLOR_MAPPER = {
    'd': DIAMOND,
    's': SPADE,
    'c': CLOVER,
    'h': HEART,
    'D': DIAMOND,
    'S': SPADE,
    'C': CLOVER,
    'H': HEART
}
VALUE_MAPPER = {
    't': TEN,
    'j': JACK,
    'q': QUEEN,
    'k': KING,
    'a': ACE,
    'T': TEN,
    'J': JACK,
    'Q': QUEEN,
    'K': KING,
    'A': ACE
}