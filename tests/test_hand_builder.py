from overbridge.card.hand_builder import HandBuilder

def test_build_hand_only_spade():
    # Given
    spades = '23456789TJQKA'

    # When
    hand = HandBuilder().add_spades(spades)

    # Then
    assert len(hand) == 13
    assert len(hand.spades) == 13
    assert len(set(hand.spades)) == 13

def test_build_hand_only_clover():
    # Given
    clovers = '23456789TJQKA'

    # When
    hand = HandBuilder().add_clovers(clovers)

    # Then
    assert len(hand) == 13
    assert len(hand.clovers) == 13
    assert len(set(hand.clovers)) == 13

def test_build_hand_only_diamond():
    # Given
    diamonds = '23456789TJQKA'

    # When
    hand = HandBuilder().add_diamonds(diamonds)

    # Then
    assert len(hand) == 13
    assert len(hand.diamonds) == 13
    assert len(set(hand.diamonds)) == 13

def test_build_hand_only_heart():
    # Given
    hearts = '23456789TJQKA'

    # When
    hand = HandBuilder().add_hearts(hearts)

    # Then
    assert len(hand) == 13
    assert len(hand.hearts) == 13
    assert len(set(hand.hearts)) == 13

def test_full_hand_builder():
    # Given
    hearts   = '234'
    spades   = '567'
    diamonds = '89TJ'
    clovers  = 'QKA'

    # When
    hand = HandBuilder()\
            .add_hearts(hearts)\
            .add_diamonds(diamonds)\
            .add_clovers(clovers)\
            .add_spades(spades)\
            .build()

    # Then
    assert '2h' in hand
    assert '3h' in hand
    assert '4h' in hand

    assert '5s' in hand
    assert '6s' in hand
    assert '7s' in hand

    assert '8d' in hand
    assert '9d' in hand
    assert 'td' in hand
    assert 'jd' in hand

    assert 'qc' in hand
    assert 'kc' in hand
    assert 'ac' in hand

def test_full_hand_builder_from_file():
    # Given
    filename = 'tests/hand_file.yml'
    handname = 'hand_one'

    # When
    hand = HandBuilder().from_file(filename, handname)

    # Then
    assert '2h' in hand
    assert '3h' in hand
    assert '4h' in hand

    assert '5s' in hand
    assert '6s' in hand
    assert '7s' in hand

    assert '8d' in hand
    assert '9d' in hand
    assert 'td' in hand
    assert 'jd' in hand

    assert 'qc' in hand
    assert 'kc' in hand
    assert 'ac' in hand


def test_hand_highcard_points():
    # Given
    filename = 'tests/hand_file.yml'
    handname = 'hand_one'

    # When
    hand = HandBuilder().from_file(filename, handname)

    # Then
    assert hand.highcard_points == 10

def test_hand_length_points():
    # Given
    filename = 'tests/hand_file.yml'
    handname = 'hand_two'

    # When
    hand = HandBuilder().from_file(filename, handname)

    # Then
    assert hand.length_points == 3

def test_hand_has_all_colors():
    # Given
    filename = 'tests/hand_file.yml'
    handname = 'hand_three'

    # When
    hand = HandBuilder().from_file(filename, handname)

    # Then
    assert len(hand._cards) == 4

def test_hand_distribution_points():
    # Given
    filename = 'tests/hand_file.yml'
    handname = 'hand_three'

    # When
    hand = HandBuilder().from_file(filename, handname)

    # Then
    assert hand.distribution_points == 6