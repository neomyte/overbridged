from overbridge.card import (
    interpreter,
    HEART,
    CLOVER,
    DIAMOND,
    SPADE,
    JACK,
    QUEEN,
    KING,
    ACE,
    TEN,
    COLOR,
    VALUE
)

def test_read_a_single_card_2H():
    # Given
    card = '2h'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == HEART
    assert card[VALUE] == 2

def test_read_a_single_card_3C():
    # Given
    card = '3c'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == CLOVER
    assert card[VALUE] == 3

def test_read_a_single_card_4D():
    # Given
    card = '4d'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == DIAMOND
    assert card[VALUE] == 4

def test_read_a_single_card_5S():
    # Given
    card = '5s'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == SPADE
    assert card[VALUE] == 5

def test_read_a_single_card_TS():
    # Given
    card = 'ts'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == SPADE
    assert card[VALUE] == TEN

def test_read_a_single_card_JS():
    # Given
    card = 'js'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == SPADE
    assert card[VALUE] == JACK

def test_read_a_single_card_QC():
    # Given
    card = 'qc'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == CLOVER
    assert card[VALUE] == QUEEN

def test_read_a_single_card_KH():
    # Given
    card = 'kh'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == HEART
    assert card[VALUE] == KING

def test_read_a_single_card_AD():
    # Given
    card = 'ad'

    # When
    card = interpreter.read_card(card)

    # Then
    assert card[COLOR] == DIAMOND
    assert card[VALUE] == ACE