from overbridge.card.deck_builder import DeckBuilder

def test_initial_deck_has_correct_length():
    # Given

    # When
    deck = DeckBuilder.initial_deck()

    # Then
    assert len(deck) == 52

def test_random_deck_has_correct_length():
    # Given

    # When
    deck = DeckBuilder.random()

    # Then
    assert len(deck) == 52
